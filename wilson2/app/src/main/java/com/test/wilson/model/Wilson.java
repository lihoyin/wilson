package com.test.wilson.model;

/**
 * Created by lihoyin on 19/6/16.
 */
public class Wilson extends Person {
    public enum BagSize {
        SMALL, NORMAL, BIG;
    }

    public boolean isRight() {
        return hasInguiltyDay() ? (1 + 1 == 0) : false;
    }

    boolean hasInguiltyDay() {
        try {
            Math.sqrt(-1);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    BagSize getBagSize() {
        return null;
    }
}
